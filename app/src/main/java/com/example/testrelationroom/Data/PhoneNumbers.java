package com.example.testrelationroom.Data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import com.example.testrelationroom.Entities.Person;

@Entity(tableName = "phone_table",
        foreignKeys = @ForeignKey(
                entity = Person.class,
                parentColumns = "id",
                childColumns = "personID"))
public class PhoneNumbers {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int number;
    private long personID;

    public PhoneNumbers(int number, long personID) {
        this.number = number;
        this.personID = personID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public long getPersonID() {
        return personID;
    }
}
